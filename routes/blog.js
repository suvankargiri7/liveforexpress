const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const helpers = require('../helpers/index');
const blog = require('../schemas/blogSchema');
const dotenv = require('dotenv');
const envResult = dotenv.config({ path: './environments/'+process.env.NODE_ENV+'.env' });
const appName = envResult.parsed.APP_NAME;
const routeName = 'blog';


router.get('/', (req, res) => {
    res.send('This is '+appName+' '+routeName+' api');
})

/** get all blogs api */
router.get('/all', (req, res) => {
    var query = blog.find({ status: 0 }, null, {})
    query.exec(function (err, docs) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(docs)});
        }
    });
})

router.get('/:uuid', (req, res) => {
    var query = blog.find({ uuid: req.params.uuid, status: 0 }, null, {})
    query.exec(function (err, docs) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(docs)});
        }
    });
})

/** blog create api */
//blog status: 0:request came 1:request approved 2: request reject
router.post('/create', (req, res) => {
    var newblog  = new blog({
        _id: new mongoose.Types.ObjectId(),
        uuid: helpers.randomString(20, '12345abcde'), 
        title: "Jamboo Creation",
        author: "Suvankar Giri",
        category: "Travel",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        status: 0,
        created: Date.now()
    });
    newblog.save(function(err,doc) {
        if(err) {
            console.log(err);
            res.send('ERROR!');
        }
        else {
            console.log(doc);
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(doc)});
        }
    });
})

/** blog edit api */
router.post('/edit', (req, res) => {
    var query = blog.find({ uuid: '4d35dea5a1545b53bd4a' }, null, {})
    query.exec(function (err, doc) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(doc)});
        }
    });
})


module.exports = router;


