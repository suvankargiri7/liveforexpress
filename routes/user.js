const express = require('express');
const { check, validationResult } = require("express-validator/check");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const dbUtil = require('../middlewares/db');
const mongoose = require('mongoose');

const helpers = require('../helpers/index');
const router = express.Router();
const dotenv = require('dotenv');
const User = require('../schemas/userSchema');
const envResult = dotenv.config({ path: './environments/' + process.env.NODE_ENV + '.env' });
const appName = envResult.parsed.APP_NAME;
const routeName = 'user';


router.get('/', (req, res) => {
    res.send('This is ' + appName + ' ' + routeName + ' api');
})

/** get all user api */
router.get('/all', (req, res) => {

})

/** users create api */
//users status: 0:request came 1:request approved 2: request reject
router.post('/create', (req, res) => {
    var db = dbUtil.getDb();
    db.collection('works').insert(req.body, (err, result) => {
        if (err) return console.log(err)
        res.send({ status: 'SUCCESS', msg: 'saved to database' })
    })
})

/** user edit api */
router.post('/edit', (req, res) => {

})

router.post('/signup', [
    check("firstname", "Please Enter a Valid First name")
        .not()
        .isEmpty(),
    check("lastname", "Please Enter a Valid First name")
        .not()
        .isEmpty(),
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password").isLength({ min: 6 })
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const {
        firstname,
        lastname,
        email,
        password
    } = req.body;
    try {
        let user = await User.findOne({
            email
        });
        if (user) {
            return res.status(400).json({
                msg: "User Already Exists"
            });
        }

        user = new User({
            _id: new mongoose.Types.ObjectId(),
            uuid: helpers.randomString(20, '12345abcde'),
            firstname: firstname,
            lastname: lastname,
            email: email,
            password: password,
            is_admin: 0,
            created: Date.now()
        });
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        await user.save();

        const payload = {
            user: {
                id: user._id
            }
        };

        jwt.sign(
            payload,
            "randomString", {
            expiresIn: 10000
        },
            (err, token) => {
                if (err) throw err;
                res.status(200).json({
                    token
                });
            }
        );
    } catch (err) {
        console.log(err.message);
        res.status(500).send("Error in Saving");
    }
})

router.post('/login', [
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password").isLength({
        min: 6
    })
], async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const { email, password } = req.body;
    try {
        let user = await User.findOne({
            email
        });
        if (!user)
            return res.status(400).json({
                message: "User Not Exist"
            });

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch)
            return res.status(400).json({
                message: "Incorrect Password !"
            });

        const payload = {
            user: {
                uuid: user.uuid,
            }
        };

        jwt.sign(
            payload,
            "liveForAuth",
            {
                expiresIn: 3600
            },
            (err, token) => {
                if (err) throw err;
                res.status(200).json({
                    token
                });
            }
        );
    } catch (e) {
        console.error(e);
        res.status(500).json({
            message: "Server Error"
        });
    }
})

router.get('/validate', async (req, res) => {
    console.log(req.headers.token);
    
    jwt.verify(req.headers.token, "liveForAuth", (err, decodedToken) => 
    {
      if (err || !decodedToken)
      {
        res.status(200).json({
            err
        });
      }

      res.status(200).json({
        decodedToken
    });
    })
})


module.exports = router;


