const express = require('express');
const router = express.Router();
var mongoose = require('mongoose');
var helpers = require('../helpers/index');
var work = require('../schemas/workSchema');
const dotenv = require('dotenv');
const envResult = dotenv.config({ path: './environments/'+process.env.NODE_ENV+'.env' });
const appName = envResult.parsed.APP_NAME;
const routeName = 'work';

router.get('/', (req, res) => {
    res.send('This is '+appName+' '+routeName+' api');
})

/** get all work api */
router.get('/all', (req, res) => {
    var query = work.find({ status: 0 }, null, {})
    query.exec(function (err, docs) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(docs)});
        }
    });
})

router.get('/:uuid', (req, res) => {
    var query = work.find({ uuid: req.params.uuid, status: 0 }, null, {})
    query.exec(function (err, docs) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(docs)});
        }
    });
})


/** work create api */
//work status: 0:request came 1:request approved 2: request reject
router.post('/create', (req, res) => {
    var new_work = new work({
        _id: new mongoose.Types.ObjectId(),
        uuid: helpers.randomString(20, '12345abcde'), 
        title: "liveFor Creation",
        subtitle:"liveFor Creation",
        author: "Suvankar Giri",
        category: "Travel",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        tools: "Test+test+test",
        status: 0,
        created: Date.now()
    });
    new_work.save(function(err) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.send('SUCCESS!');
        }
    });
})

/** work edit api */
router.post('/edit', (req, res) => {
    var query = blog.find({ uuid: '4d35dea5a1545b53bd4a' }, null, {})
    query.exec(function (err, doc) {
        if(err) {
            res.send('ERROR!');
        }
        else {
            res.status(200).send({status:'SUCCESS!', data: JSON.stringify(doc)});
        }
    });
})


module.exports = router;


