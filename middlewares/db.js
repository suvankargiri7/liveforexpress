const dotenv = require('dotenv');
const dbConfig = dotenv.config({ path: './environments/' + process.env.NODE_ENV + '.env' });
const MongoClient = require( 'mongodb' ).MongoClient;
const Mongoose = require('mongoose');
const url = 'mongodb://'+dbConfig.parsed.DB_HOST+':'+dbConfig.parsed.DB_PORT+'/'+dbConfig.parsed.DB_NAME;

module.exports = {

  connectToServer: function() {
    return Mongoose.connect( url,{user: dbConfig.parsed.DB_USER, pass: dbConfig.parsed.DB_PASS, useNewUrlParser: true, useUnifiedTopology: true});
  },

};