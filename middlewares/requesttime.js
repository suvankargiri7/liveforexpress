var requestTime = function (req, res, next) {
    req.requestTime = Date.now()
    console.log('Requested Time: '+req.requestTime);
    next()
}

module.exports = requestTime;