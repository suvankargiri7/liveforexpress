const express = require ('express');
const dotenv = require('dotenv');
const cors = require('cors');
const bodyParser= require('body-parser');
const envResult = dotenv.config({ path: './environments/'+process.env.NODE_ENV+'.env' });
const app = express();
const port = envResult.parsed.APP_PORT;
const appName = envResult.parsed.APP_NAME;

/** Import Middleware */
const db = require('./middlewares/db');
var requestTime = require('./middlewares/requesttime');

app.use(cors());
app.use(requestTime);
app.use(bodyParser.json());

/** Import Route Dependencies */
var blog = require('./routes/blog');
var work = require('./routes/work');
var user = require('./routes/user');

app.use('/blog', blog);
app.use('/work', work);
app.use('/user', user);

//app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req,res) => {
   res.send('Hello World!! This is '+appName+' Api Service.');
});

console.log('MongoDB URL:'+'mongodb://'+envResult.parsed.DB_USER+':'+envResult.parsed.DB_PASS+'@'+envResult.parsed.DB_HOST+':'+envResult.parsed.DB_PORT+'/'+envResult.parsed.DB_NAME);

db.connectToServer().then( async() => {
    app.listen(port, (err) => {
        if (err) {
            console.log(err);
            throw err; //
        }
        console.log(`${appName} listening on port ${port}!`);
    });
});