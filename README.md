Create a new repository
git clone git@gitlab.com:suvankargiri7/liveforexpress.git
cd liveforexpress
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:suvankargiri7/liveforexpress.git
git add .
git commit -m "Initial commit"
git push -u origin master

# Setup Environment

Windows: Navigate to  `https://nodejs.org/en/download/`. Download the latest .msi file and install as administrator. Run command to check install properly `node -v` and `npm -v`. 

Linux: To add the repository for the latest version of Node.js.Run `curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -` and install Nodejs and NPM on your system using the command `sudo apt-get install -y nodejs`. Run command to check install properly `node --version` and `npm --version`.

# liveforexpress

This project is the backend server for my own brand name: liveFor with npm version 6.9.0 and node version v10.16.0.

## Installation

Run `npm install`.

## Development server

Run `npm run startdevserver`. Navigate to `http://localhost:3002/`. The service will give you a response.
