var mongoose = require('mongoose');

var workSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    uuid: String,
    title: String,
    subtitle: String,
    /*author: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },*/
    author: String,
    category: String,
    description: String,
    tools: String,
    /*banner_image: { data: Buffer, contentType: String },
    more_images: [
        { data: Buffer, contentType: String }
    ],*/
    status: Number,
    created: { 
        type: Date,
    },
    last_updated: { 
        type: Date,
        default: Date.now
    }
});
 
var Work = mongoose.model('Work', workSchema);
 
module.exports = Work;