var mongoose = require('mongoose');
 
var blogSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    uuid: String,
    title: String,
    /*author: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },*/
    author: String,
    category: String,
    content: String,
    /*banner_image: { data: Buffer, contentType: String },
    more_images: [
        { data: Buffer, contentType: String }
    ],*/
    status: Number,
    created: { 
        type: Date,
    },
    last_updated: { 
        type: Date,
        default: Date.now
    }
});
 
var Blog = mongoose.model('Blog', blogSchema);
 
module.exports = Blog;